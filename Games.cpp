#include "Games.h"
//Shawn Odom. CS162. Program 4.
// This file will hold on my functions that I will call
// in main from my class defintion.



// The constructor which initializes
// all my variables/data members
// from my class 
Game_List::Game_List()
{
	cout << "\nHow many videogames would you like to include in your list?: ";
	cin >> array_size;
	cin.ignore(100, '\n');

	array = new videogame[array_size];
	num_videogames = 0;
	pricepoint = 0;
	search_genre[GENRE];
}
// Allow the user to enter a 
// new videogame including all
// of its information and
// keep track of the number of videogames
// entered by the user
void Game_List::enter_new_videogame()
{
	if (num_videogames < array_size)
	{

		cout << "You selected to enter a new videogame, please fill out the following information: ";
		cout << "\nPlease enter the title: ";
		cin.get(array[num_videogames].game_name, TITLE, '\n');
		cin.ignore(100, '\n');

                cout << "\nPlease enter information about the game: ";
                cin.get(array[num_videogames].game_info, INFO, '\n');
                cin.ignore(100, '\n');

		cout << "\nPlease enter the genre: ";
                cin.get(array[num_videogames].game_genre, GENRE, '\n');
                cin.ignore(100, '\n');

		cout << "\nPlease enter the company name: ";
                cin.get(array[num_videogames].game_company, COMPANY, '\n');
                cin.ignore(100, '\n');

                cout << "\nPlease enter the cost of the game (to the nearest whole number): ";
                cin >> array[num_videogames].game_cost;
                cin.ignore(100, '\n');

		++num_videogames;

	}
	else
	{
		cout << "List is full, cannot enter anymore games." << endl;
	}
	
}
// Display games of a certain
// genre type chosen by the
// user
void Game_List::display_game_by_genre()
{
	cout << "\nEnter the genre you are looking for: ";
	cin.get(search_genre, GENRE, '\n');
	cin.ignore(100,'\n');

	bool match = false;

	cout << "\nGames that are " << search_genre << " genre are: " << endl;

	for (int i = 0; i < num_videogames; ++i)
	{
		if (strcmp(array[i].game_genre, search_genre) == 0)
		{
			cout << "\nTitle: " << array[i].game_name << endl;
			cout << "Description: " << array[i].game_info << endl;
		        cout << "Genre: " << array[i].game_genre << endl;
			cout << "Company: " << array[i].game_company << endl;
			cout << "Cost: $" << array[i].game_cost << endl << endl;
			
			match = true;
		}
	
	}

	if (!match)
	{
		cout << "No games matched with the genre entered. Please try again." << endl;
	}
}
// Will display all games
// entered into the list
// so far by the user
void Game_List::display_all_games()
{
	cout << "The following are all the games you have entered so far:" << endl;

	for (int i = 0; i < num_videogames; ++i)
	{
		 cout << "\nTitle: " << array[i].game_name << endl;
                 cout << "Description: " << array[i].game_info << endl;
                 cout << "Genre: " << array[i].game_genre << endl;
                 cout << "Company: " << array[i].game_company << endl;
		 cout << "Cost: $" << array[i].game_cost << endl << endl;
	}
}
// Will display games under
// the certain pricepoint chosen
// by the user
void Game_List::display_games_under_price()
{
	cout << "\nEnter the pricepoint you are looking for (whole number): $";
        cin >> pricepoint;
        cin.ignore(100,'\n');

        bool match = false;

        cout << "\nGames that are under or equal to $" << pricepoint << " are: " << endl;

        for (int i = 0; i < num_videogames; ++i)
        {
                if (array[i].game_cost <= pricepoint) 
                {
                        cout << "\nTitle: " << array[i].game_name << endl;
                        cout << "Description: " << array[i].game_info << endl;
                        cout << "Genre: " << array[i].game_genre << endl;
                        cout << "Company: " << array[i].game_company << endl;
                        cout << "Cost: $" << array[i].game_cost << endl << endl;

			match = true;
                }

        }

        if (!match)
        {
                cout << "No games on the list are under that pricepoint. Please try again." << endl;
        }

}
// Will deallocate the new memory that was
// creater to ensure to memory leaks occur.
Game_List::~Game_List()
{
	delete [] array;
}
