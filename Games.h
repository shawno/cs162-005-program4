//The purpose of this file to hold the includes, classes, and structures.
#include <iostream>
#include <cstring>
#include <iomanip>
#include <cctype>
using namespace std;

const int TITLE = 41;
const int INFO = 101;
const int GENRE = 31;
const int COMPANY = 41;

struct videogame
{
	char game_name[TITLE];
	char game_info[INFO];
	char game_genre[GENRE];
	char game_company[COMPANY];
	int game_cost;
};

class Game_List
{
	public:
		Game_List();
		~Game_List();
		void enter_new_videogame();
		void display_game_by_genre();
		void display_all_games();
		void display_games_under_price();
	private:
		videogame * array;
		int num_videogames;
		int array_size;
		int pricepoint;
		char search_genre[GENRE];
};
