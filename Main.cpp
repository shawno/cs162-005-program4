//This file will hold my menu and call all my functions
#include "Games.h"
void display_menu();

int main()
{
	// Intialize Variables
	int user_response = 0; //User response to menu options
	Game_List videogame_list; //Start a videogame list

	//Welcome the user to my program
	cout << "Welcome to my program, the purpose is to "
	     << "allow you to store a list of\n videogames with their"
	     << " descriptions and other relevant information." << endl;

	// Menu Loop
	while (user_response != 5)
	{
		// Display menu and allow user to choose option
		display_menu();
		cout << "\nPlease select one of the options listed above: ";
		cin >> user_response;
		cin.ignore(100,'\n');
		
		if (user_response == 1) // Enter a new game to the list
		{
			videogame_list.enter_new_videogame();
		}
		else if (user_response == 2) //Display all games attatched to the list
		{
			videogame_list.display_all_games();
		}
		else if (user_response == 3) // Display all games with a certain genre
		{
			videogame_list.display_game_by_genre();
		}
		else if (user_response == 4) // Display all games under a certain price
		{
			videogame_list.display_games_under_price();
		}
		else if (user_response == 5) //Quit and deallocate memory
		{
			return 0;
		}
		else
		{
			cout << "\nError, not valid input, please try again." << endl;
		}
	}
	return 0;
}
// Visually display the menu, showing the user
// what options they have to choose from
void display_menu()
{
	cout << "\n1. Enter new video game\n2. Display all videogames entered so far\n"
	     << "3. Display games of a certain genre\n4. Display games under a certain price\n"
	     << "5. Quit" << endl;
}
